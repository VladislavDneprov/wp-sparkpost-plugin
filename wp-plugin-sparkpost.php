<?php
/*
   Plugin Name: Sparkpost Mailpoet integrate
   Plugin URI: #
   Version: 0.1
   Author: Jaguar-team
   Description: Sparkpost Mailpoet integrate
   Text Domain: Sparkpost Mailpoet integrate
   License: GPLv3
  */

define('PLUGIN_URL', plugin_dir_url(__FILE__));
define('PLUGIN_IMG_URL', PLUGIN_URL.'img/');
define('PLUGIN_CSS_URL', PLUGIN_URL.'css/');
define('PLUGIN_JS_URL', PLUGIN_URL.'js/');
define('PLUGIN_DIR_LOG', __DIR__.'/logs');
define('PLUGIN_DOWNLOAD_FILE_LOG', PLUGIN_URL.'core/download-log-file.php');

define('CRON_URL', PLUGIN_URL.'core/cron/update.php');

require_once ("core/class/WpPluginSparkpost.php");

if (class_exists('WpPluginSparkpost')) {

    /** require */
    require_once ("core/class/Sparkpost.php");
    require_once ("core/class/SparkpostMailpoetIntegration.php");

    register_activation_hook(__FILE__, array('WpPluginSparkpost', 'activate'));
    register_deactivation_hook(__FILE__, array('WpPluginSparkpost', 'deactivate'));

    $wcIntagrate = new WpPluginSparkpost();

}
<?php
/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 10.03.2017
 * Time: 17:06
 */

global $wpdb;

$table_name = $wpdb->prefix ."sparkpost_mailpoet_cron";

if ($wpdb->get_var("show tables like '$table_name'") == $table_name) {
    $sql = "DROP TABLE " . $table_name;

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    $wpdb->query($sql);
}

/** delete options */
delete_option('wsp_api_key');
delete_option('wsp_start_date');
delete_option('wsp_end_date');
delete_option('wsp_type_emails');

<?php
/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 10.03.2017
 * Time: 16:34
 */

require_once '../../../../../wp-load.php';

/** init */
set_time_limit(0);
define('CRON_DATE', date('Y-m-d H:i:s'));


$object_integrate = new SparkpostMailpoetIntegration();
$object_integrate->updateSubscribeUsers();
<?php
/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 10.03.2017
 * Time: 17:06
 */
global $wpdb;

$table_name = $wpdb->prefix ."sparkpost_mailpoet_cron";
if ($wpdb->get_var("show tables like '$table_name'") != $table_name) {

    $sql = "CREATE TABLE " . $table_name . " (
        id                  mediumint(9)    NOT NULL AUTO_INCREMENT,
        quantity            BIGINT          NULL,
        quantity_real       BIGINT          NULL,
        type                LONGTEXT        NULL,
        api_key             VARCHAR(255)    NULL,
        log_file            VARCHAR(255)    NULL,
        date_start          DATETIME        NULL,
        date_end            DATETIME        NULL,
        date                DATETIME        NULL,               
        UNIQUE KEY id (id)
    );";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}
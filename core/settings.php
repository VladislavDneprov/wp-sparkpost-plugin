<?php $type_emails = get_option('wsp_type_emails'); ?>


<?php if ($this->notes): ?>
    <?php foreach ($this->notes as $number => $note): ?>
        <div class="<?= $note['status'] ?> notice notice-<?= $note['status'] ?> is-dismissible">
            <p><?= $note['message'] ?></p>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<div id="wp-plugin-sparkpost" class="wrap">
    <h1>Sparkpost</h1>
    <form action="" method="POST">
        <table class="form-table">
            <tbody>
                <tr>
                    <th>Sparkpost API Key</th>
                    <td>
                        <input name="setting[wsp_api_key]" type="text" value="<?= get_option('wsp_api_key'); ?>" placeholder="Enter your Sparkpost's api key" class="regular-text" />
                        <p class="description">Enter your Mailpoet API Key</p>
                    </td>
                </tr>
                <tr>
                    <th>Start date</th>
                    <td>
                        <?php if($this->last_date_cron): ?>
                            <code><?= $this->last_date_cron; ?></code> - <span class="description">This is a last date of Cron.</span>
                        <?php else: ?>
                            <input name="setting[wsp_start_date]" placeholder="Start date for Sparkpost request" type="text" value="<?= get_option('wsp_start_date'); ?>" class="regular-text" />
                            <p class="description">This option is enter only once.</p>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th>End date</th>
                    <td><code><?= date('Y-m-d H:i:s'); ?></code> - <span class="description">This is current Date.</td>
                </tr>
                <tr>
                    <th>What kind emails to check</th>
                    <td>
                        <fieldset>
                            <label for="type_email_bounce">
                                <input type="checkbox" name="setting[wsp_type_emails][]" id="type_email_bounce" value="bounce" <?= (in_array('bounce', $type_emails) ? 'checked="checked"' : ''); ?> />
                                bounce
                            </label>
                            <br>
                            <label for="type_email_complaint">
                                <input type="checkbox" name="setting[wsp_type_emails][]" id="type_email_complaint" value="spam_complaint" <?= (in_array('spam_complaint', $type_emails) ? 'checked="checked"' : ''); ?> />
                                Spam complaint
                            </label>
                            <br>
                            <label for="type_email_police_rejection">
                                <input type="checkbox" name="setting[wsp_type_emails][]" id="type_email_police_rejection" value="policy_rejection" <?= (in_array('policy_rejection', $type_emails) ? 'checked="checked"' : ''); ?> />
                                Police rejection
                            </label>
                            <br>
                            <label for="type_email_generation_rejection">
                                <input type="checkbox" name="setting[wsp_type_emails][]" id="type_email_generation_rejection" value="generation_rejection" <?= (in_array('generation_rejection', $type_emails) ? 'checked="checked"' : ''); ?> />
                                Generation rejection
                            </label>
                            <br>
                            <label for="type_email_out_of_brand">
                                <input type="checkbox" name="setting[wsp_type_emails][]" id="type_email_out_of_brand" value="out_of_band" <?= (in_array('out_of_band', $type_emails) ? 'checked="checked"' : ''); ?> />
                                Out of band
                            </label>
                            <br>
                            <label for="type_email_generation_failure">
                                <input type="checkbox" name="setting[wsp_type_emails][]" id="type_email_generation_failure" value="generation_failure" <?= (in_array('generation_failure', $type_emails) ? 'checked="checked"' : ''); ?> />
                                Generation failure
                            </label>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit">
            <input type="submit" class="button button-primary" value="Save">
        </p>
    </form>
    <!---
    <div>
        <p class="description">If you want to update users' subscribe status in the Mailpoet plugin, press the button below.</p>
        <button id="start_update_status" class="button button-primary">Start</button>
    </div>-->
    <div>
        <p class="description">Insert the link below to CRON TASK to update users' subscribe status automatically.</p>
        <code><?= CRON_URL; ?></code>
    </div>

    <div>
        <h3>Cron operations</h3>
        <table class="wp-list-table widefat fixed striped pages">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Responses quantity <span class="description">(from Mailpoet)</span></th>
                    <th>Unsubscribed users quantity</th>
                    <th>Types quantity</th>
                    <th>Start date</th>
                    <th>End date</th>
                    <th>API Key</th>
                    <th>Log file</th>
                    <th>Cron date</th>
                </tr>
            </thead>
            <tbody>
                <?php if($this->actions_cron): ?>
                    <?php foreach ($this->actions_cron as $number_action => $action): ?>
                        <tr>
                            <td><?= $action->id; ?></td>
                            <td><?= $action->quantity; ?></td>
                            <td><?= $action->quantity_real; ?></td>
                            <td>
                                <?php if (json_decode($action->type)): $types = json_decode($action->type); ?>
                                    <?php foreach ($types as $name_type => $count_type): ?>
                                        <p><?= $name_type; ?>: <code><?= $count_type; ?></code></p>
                                    <?php endforeach; ?>
                                    <?php else: ?>
                                        <p><code>No types</code></p>
                                <?php endif; ?>
                            </td>
                            <td><?= $action->date_start; ?></td>
                            <td><?= $action->date_end; ?></td>
                            <td><code><?= $action->api_key; ?></code></td>
                            <td>
                                <?php if ($action->log_file): ?>
                                    <a href="<?= PLUGIN_DOWNLOAD_FILE_LOG.'?file='.$action->log_file; ?>" title="Download">Download</a>
                                <?php else: ?>
                                    --
                                <?php endif; ?>
                            </td>
                            <td><?= $action->date; ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr class="no-items">
                        <td colspan="3" class="colspanchange" colspan="5">Data is empty.</td>
                    </tr>
                <?php endif; ?>
            </tbody>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Responses quantity <span class="description">(from Mailpoet)</span></th>
                <th>Unsubscribed users quantity</th>
                <th>Types quantity</th>
                <th>Start date</th>
                <th>End date</th>
                <th>API Key</th>
                <th>Log file</th>
                <th>Cron date</th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
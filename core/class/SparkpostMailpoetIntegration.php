<?php

/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 09.03.2017
 * Time: 17:29
 */
class SparkpostMailpoetIntegration
{
    protected $wpdb;

    protected $table;

    protected $log_file;

    public function __construct()
    {
        global $wpdb;
        $this->wpdb = $wpdb;

        $table_name = $this->wpdb->prefix ."wysija_user";
        $this->table = $table_name;

        if ($this->wpdb->get_var("show tables like '$table_name'") != $table_name) {
            die('Mailpoet plugin is not active.');
        }
    }

    public function updateSubscribeUsers()
    {
        $object_sparkpost = new Sparkpost();
        $data = $object_sparkpost->getMessageEvents();

        if (!$data) {
            return false;
        }

        /** init log */
        $this->setLogFile();

        $result = array(
            'quantity'          => $data->total_count,
            'quantity_real'     => 0,
            'type'              => array(),
            'api_key'           => $object_sparkpost->getApiKey(),
            'log_file'          => $this->log_file,
            'date_start'        => $object_sparkpost->getStartDate(),
            'date_end'          => $object_sparkpost->getEndDate(),
            'date'              => CRON_DATE,
        );

        foreach ($data->results as $key => $value) {

            /** add random user */
            /**if ($key != 100) {
                $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
                $user_id = wp_create_user( 'Test'.$key, $random_password, $value->rcpt_to );
            }**/

            $user = $this->getMailpoetUser($value->rcpt_to);

            if ($user) {
                $this->updateSubscribeMailpoetUser($user->user_id);

                /** add type */
                if (isset($result['type'][$value->type])) {
                    $result['type'][$value->type] += 1;
                } else {
                    $result['type'][$value->type] = 1;
                }

                /** add to log */
                $this->addUserInfoToLogFile(array(
                    'id'        => $user->user_id,
                    'email'     => $value->rcpt_to,
                ));

                /** add quantity real */
                $result['quantity_real']++;
            }
        }

        /** encode to json */
        $result['type'] = json_encode($result['type']);

        /** add to DB */
        $this->addActionCron($result);

        return $result;
    }

    public function getAllCronActions()
    {
        $table = $this->wpdb->prefix ."sparkpost_mailpoet_cron";

        $sql = 'SELECT * FROM `'.$table.'` ORDER BY `id` DESC LIMIT 10';

        return $this->wpdb->get_results($sql);
    }

    public function getLastDateCronAction($request = false)
    {
        $table = $this->wpdb->prefix ."sparkpost_mailpoet_cron";
        if ($request) {
            $sql = 'SELECT DATE_FORMAT(MAX(`date`),\'%Y-%m-%dT%H:%i\') as `date` FROM `'.$table.'`';
        } else {
            $sql = 'SELECT MAX(`date`) as `date` FROM `'.$table.'`';
        }

        return $this->wpdb->get_var($sql);
    }

    protected function addActionCron($data)
    {
        $table = $this->wpdb->prefix ."sparkpost_mailpoet_cron";

        return $this->wpdb->insert($table, $data);
    }

    protected function setLogFile()
    {
        $name = wp_hash(wp_generate_password(15, true, true)).'.txt';

        if (!file_exists(PLUGIN_DIR_LOG)) {
            mkdir(PLUGIN_DIR_LOG);
        }

        if ((defined(CRON_DATE))) {
            $date = date('F d, Y ', strtotime(CRON_DATE));
        } else {
            $date = date('F d, Y ');
        }

        $fp = fopen(PLUGIN_DIR_LOG.'/'.$name, 'w+');
        fwrite($fp, "--------------------- $date - emails unsubscribed: ---------------------\n\r");
        fclose($fp);

        return $this->log_file = $name;
    }

    protected function addUserInfoToLogFile($user_info)
    {
        if (!$this->log_file) {
            return false;
        }

        $id = $user_info['id'];
        $email = $user_info['email'];

        $fp = fopen(PLUGIN_DIR_LOG.'/'.$this->log_file, 'a+');
        fwrite($fp, "ID $id - $email\n\r");
        fclose($fp);
    }

    protected function updateSubscribeMailpoetUser($user_id)
    {
        $data = array('status' => -1);
        $where = array('user_id' => $user_id );

        return $this->wpdb->update($this->table, $data, $where);
    }

    protected function getMailpoetUser($email)
    {
        $sql = 'SELECT `user_id`, `email` FROM `'.$this->table.'`
                WHERE `email` = "'.$email.'" AND `status` IN (0, 1)';

        return $this->wpdb->get_row($sql);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 09.03.2017
 * Time: 12:03
 */

class WpPluginSparkpost
{
    public $object_sparkpost;

    public $notes;

    protected $actions_cron;

    protected $last_date_cron;

    public function __construct()
    {
        add_action('init', array(&$this, 'init'));
        add_action('admin_menu', array(&$this, 'add_menu'));
    }

    public function init()
    {
        set_time_limit(0);

        $this->registerStyles();
        $this->registerScripts();
        $this->save();

        /**  */
        $object_sparkpost_integrate = new SparkpostMailpoetIntegration();
        $this->last_date_cron = $object_sparkpost_integrate->getLastDateCronAction();
        $this->actions_cron = $object_sparkpost_integrate->getAllCronActions();

        include_once(ABSPATH . 'wp-admin/includes/plugin.php');
        if (!is_plugin_active('wysija-newsletters/index.php')) {
            $this->notes[] = array(
                'status' => 'error',
                'message' => 'Mailpoet plugin in not active',
            );
        }

        /** check options */
        if (!get_option('wsp_api_key')) {
            $this->notes[] = array(
                'status' => 'warning',
                'message' => 'You should enter your Mailpoet API key.',
            );
        }

        if (!get_option('wsp_type_emails')) {
            $this->notes[] = array(
                'status' => 'warning',
                'message' => 'You should choose a list one type of email.',
            );
        }
        
        if (!$this->last_date_cron && !get_option('wsp_start_date')) {
            $this->notes[] = array(
                'status' => 'warning',
                'message' => 'You should enter start date for Mailpoet request.',
            );
        }
    }

    public static function activate()
    {
        require_once(__DIR__ . "/../install.php");
    }

    public static function deactivate()
    {
        require_once(__DIR__ . "/../uninstall.php");
    }

    public function add_menu()
    {

        add_menu_page(
            'Sparkpost',
            'Sparkpost',
            'manage_options',
            'integrate_menu',
            array(&$this, 'settings_print'),
            PLUGIN_URL . 'img/admin-icon.png',
            55
        );
    }

    public function registerScripts()
    {
        wp_enqueue_script(
            'sparkpost-datepicker',
            PLUGIN_JS_URL . 'wp-plugin-sparkpost.js',
            array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'),
            time(),
            true
        );

        wp_enqueue_script(
            'inputmask',
            PLUGIN_JS_URL . 'jquey.inputmask.bundle.min.js',
            array('jquery'),
            time(),
            true
        );
    }

    public function registerStyles()
    {
        wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
        wp_enqueue_style('wp-plugin-sparkpost', PLUGIN_CSS_URL . 'wp-plugin-sparkpost.css');
    }

    public function settings_print()
    {
        if ( !current_user_can('manage_options') ) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }
        require_once(__DIR__."/../settings.php");
    }

    public function save()
    {
        if (isset($_POST['setting'])) {
            $settings = $_POST['setting'];
            foreach ($settings as $name => $setting) {
                if ($name == 'wsp_type_emails') {
                    $settings[$name] = json_encode($setting);
                }

                update_option($name, $setting);
            }
        }

        return $this;
    }
}
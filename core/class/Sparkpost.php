<?php

/**
 * Created by PhpStorm.
 * User: vlad.dneprov
 * Date: 09.03.2017
 * Time: 13:13
 */
class Sparkpost
{
    private $url = 'https://api.sparkpost.com/api';

    private $version = 'v1';

    protected $api_key;

    protected $start_date;

    protected $end_date;

    protected $type_emails;

    protected $headers;

    protected $current_request;

    protected $current_method;

    protected $current_params;

    public function __construct()
    {
        try {
            $object_sparkpost_integrate = new SparkpostMailpoetIntegration();

            /** init */
            if (!get_option('wsp_api_key')) {
                throw new Exception('API Key is not defined.');
            }
            $this->api_key = get_option('wsp_api_key');

            if (!get_option('wsp_type_emails')) {
                throw new Exception('Types email in not defined.');
            }
            $this->type_emails  = get_option('wsp_type_emails');

            if ($object_sparkpost_integrate->getLastDateCronAction()) {
                $this->start_date = $object_sparkpost_integrate->getLastDateCronAction(true);
            } else {
                $this->start_date   = get_option('wsp_start_date');
            }

            if (!$this->start_date) {
                throw new Exception('Start date is not defined.');
            }

            $this->end_date = date('Y-m-d\TH:i');

            $duration = strtotime($this->end_date) - strtotime($this->start_date);
            if ($duration < 60) {
                throw new Exception('Sorry. You will can send request after '.(60 - $duration).' seconds.');
            }

            $this->setHeaders();

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function getMessageEvents()
    {
        $this->current_method = 'message-events';
        $this->current_params = array(
            'events'    => implode(',', $this->type_emails),
            //'page'      => 2,
            //'per_page'  => 10000,
            'from'      => $this->start_date,
            'to'        => $this->end_date,
        );

        return $this->setRequest($this->current_method, $this->current_params)->send();
    }

    public function getEndDate()
    {
        return $this->end_date;
    }

    public function getStartDate()
    {
        return $this->start_date;
    }

    public function getApiKey()
    {
        return $this->api_key;
    }

    protected function setRequest($method, $params)
    {
        $params_inline = '';
        if ($params && is_array($params)) {
            $number_param = 1;
            foreach ($params as $name => $value) {
                if ($number_param != 1) {
                    $params_inline .= '&'.$name.'='.$value;
                } else {
                    $params_inline .= $name.'='.$value;
                }
                $number_param++;
            }
        }

        $this->current_request = $this->url.'/'.$this->version.'/'.$method.'/?'.$params_inline;

        return $this;
    }

    protected function setHeaders()
    {
        $options = array('http' => array(
                'method'    =>  'GET',
                'header'    =>  "Authorization: ".$this->api_key."\r\n" .
                                "Content-type: application/json\r\n"
            )
        );

        $this->headers = stream_context_create($options);

        return $this;
    }

    protected function send($json = false)
    {
        if (!$this->url && !$this->headers) {
            return false;
        }

        if (!$json) {
            return json_decode(file_get_contents($this->current_request, false, $this->headers));
        } else {
            return file_get_contents($this->current_request, false, $this->headers);
        }
    }
}
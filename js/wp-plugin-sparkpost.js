/**
 * Created by vlad.dneprov on 10.03.2017.
 */

jQuery(document).ready(function(){
    jQuery('#wp-plugin-sparkpost input[name="setting[wsp_start_date]"]').inputmask(
        '9999-99-99T99:99'
    );
    jQuery('#wp-plugin-sparkpost input[name="setting[wsp_api_key]"]').inputmask(
        '****************************************'
    );
});